if ($host.Name -eq 'ConsoleHost') {
    Import-Module PSReadline

    Set-PSReadlineOption -EditMode Emacs

    Import-Module 'posh-git'
    Import-Module 'oh-my-posh'
    Set-Theme agnoster
}

function ln ($target, $link) {
    New-Item -Path $link -ItemType SymbolicLink -Value $target
}